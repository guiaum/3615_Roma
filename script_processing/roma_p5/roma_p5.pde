/*
This is a sound file player. 
*/
import processing.io.*;
import processing.sound.*;

long lastStart = 0;
int minTime=5000;

void setup() {
    size(200,200);
    background(255);
    GPIO.pinMode(23, GPIO.INPUT);
}      


void draw() {
  //Someone is here
  if (GPIO.digitalRead(23) == GPIO.HIGH) {
      increaseVolume();
      lastStart = millis();
  }
  
  else if (GPIO.digitalRead(23) == GPIO.LOW && (millis()-lastStart)>minTime) {
      decreaseVolume();
      
  }
  

}

public void increaseVolume()
{
    try
    {
      
      String command = "amixer set Digital playback 2%+";

      Process child = Runtime.getRuntime().exec(command);
    }
    catch (IOException e)
    {
    }
    delay(50);
}

public void decreaseVolume()
{
    try
    {
      
      String command = "amixer set Digital playback 1%-";

      Process child = Runtime.getRuntime().exec(command);
    }
    catch (IOException e)
    {
    }
    delay(600);
}
