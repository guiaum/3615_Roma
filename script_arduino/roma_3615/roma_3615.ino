// LCD5110_TinyFont_View 
// Copyright (C)2015 Rinky-Dink Electronics, Henning Karlsen. All right reserved
// web: http://www.RinkyDinkElectronics.com/
//
// This program is a demo of the tiniest font.
//
// This program requires a Nokia 5110 LCD module.
//
// It is assumed that the LCD module is connected to
// the following pins:
//      SCK  - Pin 8
//      MOSI - Pin 9
//      DC   - Pin 10
//      RST  - Pin 11
//      CS   - Pin 12
//
#include <LCD5110_Graph.h>
#include <EEPROM.h>

LCD5110 myGLCD(8,9,10,11,12);

//Capteur IR
int sensorPin = A0;
int sensorValue = 0;

//Stored value
int addr = 0;
int buttonPin = 7;
int limitValue = 0;

//Détection
float score = 0;
int incrementScore = 10;
int seuil = 79;

//GPIO
int GPIOpin = 5;
boolean playing = false;

extern unsigned char TinyFont[];

void setup()
{
  myGLCD.InitLCD(70);
  myGLCD.setFont(TinyFont);
  pinMode(buttonPin, INPUT_PULLUP);
  pinMode(GPIOpin, OUTPUT);
  Serial.begin(9600);
}

void loop()
{
  // read the value from the sensor:
  sensorValue = analogRead(sensorPin); 
  
  //Update of value
  if (digitalRead(7) == LOW)
  {
    EEPROM.put(addr, sensorValue);
    delay(200);
  }
  
  EEPROM.get(addr, limitValue);

  if (sensorValue>limitValue)
  {
    score += incrementScore;
  }
  
  score = (8*score)/9;
  Serial.println(score);
  if (score>seuil)
  {
      digitalWrite(GPIOpin, HIGH);
      playing = true;
  }else{
    digitalWrite(GPIOpin, LOW);
    playing = false;
  }
  myGLCD.print("From sensor: "+ String(sensorValue), LEFT, 0);
  myGLCD.print("Limit stored: "+ String(limitValue), LEFT, 12);
  myGLCD.print("Score: "+ String(score), LEFT, 24);
  myGLCD.print("Playing: "+ String(playing), LEFT, 36);
  myGLCD.update();

  delay(10);
}
