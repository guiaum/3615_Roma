# 3615_ROMA

Un petit module qui prends place dans une galerie. Lorsqu'une personne s'approche, le volume d'un wave qui est joué est monté. Inversement, le volume baisse doucement lorsque le public s'en va.

![pic1](3615-roma-1-small.jpg)

![pic2](3615-roma-2-small.jpg)

### Utilisation
Il est possible d'ajuster le seuil de détection directement sur l'arduino: une pression sur le bouton enregistre l'état actuel du capteur comme seuil de déclenchement, et l'affiche sur l'écran.
Le fichier wave est stocké sur la clé USB (bien penser à manipuler la clé USB lorsque le raspberry est éteint). Il doit impérativement avoir le même nom que celui proposé (roma.wav)

### Hardware

Capteur: Sharp GP2Y0A710K0F IR Range Sensor // 100 to 550cm

Raspberry pi 3 + [JustBoom DAC HAT](https://www.justboom.co/product/justboom-dac-hat/) (choisi car il possède une vraie sortie casque)

Arduino nano + écran Nokia 5110 + switch

L'arduino est alimenté par les pin 5V du Raspberry. Il va se brancher via un pont diviseur de tension sur la pin 23 du Raspi. Le pont diviseur utilisé: Arduino D5 --- 10k --- vers GPIO 23 --- 10k+4.7k --- GND . On passe ainsi de 5V à 2V, ce qui est suffisant pour décelencher le changement d'état.

### Software
Arduino: [lib pour nokia 5110](http://www.rinkydinkelectronics.com/library.php?id=47)
Processing: lib hardware i/o puis utilisation de l'outil "Upload to Pi" pour compiler sur arm et démarrer automatiquement.

La piste son est jouée en boucle par mplayer, ajouté à ~/.config/lxsession/LXDE/autostart pour démarrer automatiquement.

Le raspberry génére un réseau adhoc pour pouvoir l'administrer si besoin.